# codesite

A static site generator for a portfolio website.

This script was used to generate the https://code.ivysaur.me/ website between 2013 and 2020, when it was replaced by a [teafolio](https://code.ivysaur.me/teafolio) instance.

Written in PHP, Bash

## Features

- Generates static website, minimising server load and decreasing attack surface compared to dynamic server-side PHP
- Automatic thumbnailing and sprite sheet generation
- Download file attachments per-project
- One-click rebuild, one-click deploy
- Parallel generation

## Changelog

2020-05-24: v157
- Sunset release
- Rewrite `codesite` Git repository history for full public release
- Feature: Add support for autogenerating codesite projects from a Git repository (`update_git_remote.sh`)
- Feature: Support converting a codesite project into a readonly, archive Git repository (`codesite2git`)
- Feature: Support bulk pushing `codesite2git`-converted repositories to a Gitea instance

2018-12-30: v138
- (Private release only)
- Feature: Support `[git-repository]` tags to link to a custom VCS repository
- Feature: Support markdown link syntax
- Feature: Support markdown multiline comments that include code syntax highlight tag
- Enhancement: Support Go subpackages under `[go-get]` tags
- Enhancement: Strip markdown image urls
- Enhancement: Trim leading blank lines
- Fix a cosmetic issue with favicons and document rendering mode in some versions of IE

2017-10-28: v132
- Fix an issue with not applying project rename redirections in the new URL format
- Fix a cosmetic issue with classifying downloads named with numeric strings
- Fix a cosmetic issue with truncated sheilds caused by ID collisions
- [⬇️ code_v132.tar.xz](.dist-archive/code_v132.tar.xz) *(106.49 KiB)*

2017-04-23: v126
- Breaking: BASEURL is now a mandatory field
- Feature: Support `[go-get]` tags
- Feature: Canonical paths for SEO
- Feature: Use extension-less paths by default (old .html paths still supported via redirect)
- Feature: Support `[url]` tags
- Enhancement: Cache-busting parameters on homepage thumbnails
- [⬇️ code_v126.tar.xz](.dist-archive/code_v126.tar.xz) *(106.39 KiB)*

2016-04-19: v118
- Feature: Classify download artefacts by matching CHANGELOG entry
- Feature: Allow sorting by lifespan, artefacts, release entries
- Feature: Separate site generation code from site data repositories
- Enhancement: Update supplied CSS normalize script
- Enhancement: Read parallelism from number of CPUs
- [⬇️ code_v118.tar.xz](.dist-archive/code_v118.tar.xz) *(103.18 KiB)*

2015-11-08: v97
- Feature: Support BBCode b/i/spoiler/entry tags
- Feature: Add more CSS styles to allow per-site customistaion
- Feature: Use http://shields.io images where appropriate
- Feature: Optional blurbs=off, article_header={string}, shields_prefix=true configuration directives
- Fix an issue with project update-time detection
- Fix a cosmetic issue with whitespace
- [⬇️ code_v97.tar.xz](.dist-archive/code_v97.tar.xz) *(107.54 KiB)*

2015-04-05: v72
- Feature: Support redirecting old project names
- Feature: Add file hash in download URLs to prevent filename collisions
- Fix an issue generating spritesheets even if no project images are present
- Don't include `ctime` when estimating project update time
- [⬇️ code_v72.tar.xz](.dist-archive/code_v72.tar.xz) *(106.79 KiB)*

2015-04-05: v64
- Feature: Support sorting projects
- [⬇️ code_v64.tar.xz](.dist-archive/code_v64.tar.xz) *(106.45 KiB)*

2015-04-04: v54
- Feature: Support multiple code sites
- Fix an issue with parallel builds on some versions of windows
- Fix an issue corrupting URL links with multiple parameters
- Fix a cosmetic issue with page overflow
- Fix a cosmetic issue with whitespace on code elements
- [⬇️ code_v54.tar.xz](.dist-archive/code_v54.tar.xz) *(105.79 KiB)*

2014-07-02: v39
- Feature: Tags
- Feature: Generate pages in parallel
- Enhancement: Support raw HTML sections in page content
- Fix an issue with URLs containing spaces
- Fix a cosmetic issue with image thumbnail backgrounds
- Fix a cosmetic issue with download sort order
- Fix a cosmetic issue with page layout
- [⬇️ code_v39.zip](.dist-archive/code_v39.zip) *(13.11 KiB)*

2013-09-28: v13
- Initial public source code release
- [⬇️ code_v13.zip](.dist-archive/code_v13.zip) *(9.02 KiB)*

2013-09-21: v3
- Initial deployment
