(function() {
	"use strict";

	//
	// Tag support
	//
	
	var show_all = function() {
		var tr = document.querySelectorAll(".projtable tr");
		for (var i = 0, e = tr.length; i !== e; ++i) {
			tr[i].style.display = "table-row";
		}
		
		var warn = document.querySelector(".tag-filter-warn");
		warn.parentNode.removeChild(warn);
	};
	
	var show_tag = function(tag) {
		if (document.querySelector(".tag-filter-warn") !== null) {
			show_all();
		}
	
		var tr = document.querySelectorAll(".projtable tr");
		for (var i = 0, e = tr.length; i !== e; ++i) {
			tr[i].style.display = (tr[i].className.split(" ").indexOf("taggedWith-"+tag) === -1) ? "none" : "table-row";
		}
		
		var div = document.createElement("div");
		div.className = "tag-filter-warn";
		div.innerHTML = "Filtering by tag. <a>reset</a>";
		document.body.appendChild(div);
		
		document.querySelector(".tag-filter-warn a").addEventListener('click', function() {
			show_all();
			return false;
		});
	};

	var get_show_tag = function(tag) {
		return function() {
			show_tag(tag);
			return false;
		};
	};

	window.addEventListener('load', function() {
		var taglinks = document.querySelectorAll(".tag-link");
		for (var i = 0, e = taglinks.length; i !== e; ++i) {
			var tag = taglinks[i].getAttribute("data-tag");
			taglinks[i].addEventListener('click', get_show_tag(tag));
		}
	});
	
	//
	// Sort support (theme opt-in)
	//
	
	var sort_rows = function(cb) {
		var tr = document.querySelectorAll(".projtable tr");
		var items = [];
		for (var i = 0, e = tr.length; i !== e; ++i) {
			items.push([i, cb(tr[i])]);
		}
		items.sort(function(a, b) {
			return (a[1] - b[1]);
		});
		for (var i = 0, e = items.length; i !== e; ++i) {
			var el = tr[items[i][0]];
			var parent = el.parentElement;
			parent.removeChild(el);
			parent.appendChild(el);
		}
	};
	
	var sort_update = function(sort_by) {
		sort_rows(function(el) {
			return el.getAttribute(sort_by);
		});
	};
	
	window.sortUpdate = sort_update;
	
})();
