#!/bin/bash

set -eu

APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

numcpus() {
	cat /proc/cpuinfo | grep '^processor' | wc -l
}

buildsite() {
	local site="$1"

	echo "Site: ${site}"
	(
		cd "$site"

		echo "Cleaning wwwroot directory..."

		if [[ -d wwwroot ]] ; then
			rm -r wwwroot
		fi	
		mkdir -p wwwroot/{img,srv,static}
		
		echo "Copying static resources..."
		
		if [[ ! -d static ]] ; then
			mkdir static
		fi
		cp "${APP_DIR}/static/"* wwwroot/static || true
		cp static/* wwwroot/static || true
		
		for htm in footer header homepage_blurb ; do
			if [[ ! -f "${htm}.htm" ]] ; then
				touch "${htm}.htm"
			fi
		done

		echo "Building pages..."

		local threadcount=$(numcpus)
		for i in $(seq 0 "$threadcount") ; do
			"${APP_DIR}/build-worker" "$threadcount" "$i" &
		done
		wait

		echo "Site: ${site} finished."
		echo ""
	)

}

usage() {
	echo "USAGE: ./rebuild.sh path-to-siteroot"
	exit 1
}

main() {
	if [[ $# -ne 1 ]] ; then
		usage
	fi
	
	buildsite "$1"
}

main "$@"
