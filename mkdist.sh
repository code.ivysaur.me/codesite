#!/bin/bash

set -eu

tar cJvf "codesite-$(date +%s).tar.xz" \
	--exclude='sites/codesite.example.com/wwwroot' \
	rebuild.sh \
	sites/codesite.example.com \
	static \
	lib \
	--owner=0 --group=0

read -p "Press any key to continue..."

