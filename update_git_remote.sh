#!/bin/bash
#
# This script updates/rebuilds a codesite directory from a Git repository.
#

set -eu

main() {

    if [[ $# -ne 3 ]] ; then
        echo "Usage: update_git_remote DATA_DIR GIT_REMOTE_URI EXTRA_CONTENT" >&2
        exit 1
    fi
    local datadir="$1"
    local gitremote="$2"
    local extra="${3:-}"

    #

    echo "Updating ${datadir}..."

    if [[ ! -d $datadir ]] ; then
        mkdir $datadir
    fi

    local clone=$(mktemp -d)
    (
        cd "${clone}"
        git clone "${gitremote}" .
    )
    cp "${clone}/README.md" "${datadir}/README.txt"

    rm -fr "${clone}"

    # Ensure LF endings
    sed -i 's/\x0D$//' "${datadir}/README.txt"

    # Remove all shields (codesite inserts its own)
    sed -i /shields.io/d "${datadir}/README.txt"

    # Remove top-level header (codesite inserts its own)
    sed -i -re '/^#([^#])/d' "${datadir}/README.txt"

    # Remove leading blank lines (since we removed the top-level header)
    sed -i '/./,$!d' "${datadir}/README.txt"

    # Convert headings from github-flavored markdown to codesite style (\U for Uppercase)
    sed -i -re 's/^## (.+)/=\U\1=/' "${datadir}/README.txt"

    # Add extra metadata so that codesite generator can link the repository
    echo "" >> "${datadir}/README.txt"
    echo "[git]${gitremote}[/git]" >> "${datadir}/README.txt"

    # Add any extra per-repository metadata, on a line after the description
    if [[ -n "${extra}" ]] ; then
        sed -i 3i"${extra}" "${datadir}/README.txt"
    fi
}

main "$@"
