<?php

ini_set('display_errors', 'On');
date_default_timezone_set('Etc/UTC');
error_reporting(E_ALL);

require __DIR__.'/util.php';
require __DIR__.'/template.php';
require __DIR__.'/CProject.php';

define('SHIELDS_CACHE_DIR', __DIR__.'/../shields_cache/');

/**
 * Set up global defines for a given codesite project
 * Should be called once we have the root path for a codesite project.
 * 
 */
function setup_vars(string $basedir="./"): array {
	
	// Parse configuration
	if (! is_file($basedir.'config.ini')) {
		die("[FATAL] Non-file '${basedir}config.ini'!\n");
	}

	$config = @parse_ini_file(
		$basedir . 'config.ini',
		true,
		INI_SCANNER_RAW
	);
	
	if ($config === false) {
		die("[FATAL] Couldn't load '${basedir}config.ini'!\n");
	}
	
	define('BASEDIR',       $basedir);
	define('BASEURL',       trim($config['codesite']['baseurl']));
	define('SITE_TITLE',    trim($config['codesite']['title']));
	define('PAGE_THUMB_W',  intval($config['codesite']['page_thumb_w']));
	define('PAGE_THUMB_H',  intval($config['codesite']['page_thumb_h']));
	define('INDEX_THUMB_W', intval($config['codesite']['index_thumb_w']));
	define('INDEX_THUMB_H', intval($config['codesite']['index_thumb_h']));
	define('SHOW_BLURBS',   !(isset($config['codesite']['blurbs']) && $config['codesite']['blurbs'] === 'off') );
	define('ARTICLE_HEADER', (isset($config['codesite']['article_header']) ? $config['codesite']['article_header'] : 'ABOUT') );	
	define('SHIELDS_PREFIX', isset($config['codesite']['shields_prefix']));
	
	return $config;
}
