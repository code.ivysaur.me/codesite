<?php

function template($title, $content, $extra_head='') {
	ob_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?=hesc($title)?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
		<meta name="viewport" content="width=960" >
		<?=$extra_head?> 
		<link rel="icon" href="<?=BASEURL?>static/favicon.ico" type="image/x-icon">
		<link type="text/css" rel="stylesheet" href="<?=BASEURL?>static/normalize.css">
		<link type="text/css" rel="stylesheet" href="<?=BASEURL?>static/style.css">
		<script type="text/javascript" src="<?=BASEURL?>static/site.js"></script>		
	</head>
	<body>
		<div id="container">
			<div id="content">
<?=file_get_contents(BASEDIR.'/header.htm')?> 
<?=$content?> 
			</div>
		</div>
	</body>
</html>
<?php
	return ob_get_clean();
}


function listprojects() {
	// List projects

	$ls = scandir(BASEDIR.'data');
	rsort($ls);
	$projects = array();
	foreach($ls as $dirname) {
		if ($dirname[0] == '.') continue;
		if (! is_dir(BASEDIR.'data/'.$dirname)) continue;
		$matches = array();

		if (preg_match('~(?:\d+-)?(.+)~', $dirname, $matches)) {
			$projects[$dirname] = $matches[1];
		}
	}
	
	return $projects;
}

function buildprojects($id, $projects) {
	$count = 0;
	
	foreach($projects as $dirname => $projectname) {

		echo sprintf("@%1d [%3d/%3d] ".$projectname."...\n", $id, ++$count, count($projects));

		$pr = new CProject($dirname, $projectname);	
		$pr->write();
	}
}

function buildcommon() {
	
	echo "@0 [  0/  ?] Common files...\n";
	
	$projects = listprojects();

	// Build all projects

	$plist = array();

	$handles = array();
	$handle_lookup = array();
	
	$alphasort = [];
	
	foreach($projects as $dirname => $projectname) {

		$pr = new CProject($dirname, $projectname);
		$pr->genHomeImage(); // thumbnail
		
		$plist[] = $pr;

		if (is_null($pr->homeimage)) {
			$handle_lookup[$projectname] = null;
		} else {
			$handle_lookup[$projectname] = count($handles);
			$handles[] = $pr->homeimage;
		}
		
		$alphasort[] = [$pr->projname, count($plist)-1];
	}
	
	usort($alphasort, function($a, $b) {
		return strcasecmp($a[0], $b[0]);
	});
	
	$alphaidx = [];
	
	foreach($alphasort as $a) {
		$alphaidx[ $a[1] ] = count($alphaidx);
	}

	// Build homepage spritesheet
	
	if (count($handles)) {
		mkspritesheet($handles, BASEDIR.'wwwroot/logos.jpg', INDEX_THUMB_W, INDEX_THUMB_H);
		array_map('imagedestroy', $handles); // free
	}
	
	// Cache-busting stylesheet

	$style = '.homeimage-sprite { background-image: url("logos.jpg?'.md5_file(BASEDIR.'wwwroot/logos.jpg').'"); }';

	// Build index page

	ob_start();
?>

	<?php if (file_exists(BASEDIR.'homepage_blurb.htm')) { ?> 
		<!-- homepage blurb {{ -->
		<?=file_get_contents(BASEDIR.'homepage_blurb.htm')?> 
		<!-- }} --> 
	<?php } ?> 
		
		<style type="text/css">
	<?php echo $style; ?> 
		</style>

		<table id="projtable-main" class="projtable">
	<?php foreach ($plist as $i => $pr) { ?> 
			<tr class="<?=$pr->getClassAttr()?>"
				data-sort-mt="-<?=$pr->lastupdate?>"
				data-sort-ct="<?=$i?>"
				data-sort-al="<?=$alphaidx[$i]?>"
				data-sort-nr="-<?=$pr->numreleases?>"
				data-sort-nf="-<?=$pr->numDownloads()?>"
				data-sort-ls="-<?=$pr->lifespan?>"
			>
				<td>
					<a href="<?=hesc(BASEURL.urlencode($pr->projname))?>/"><?=(is_null($handle_lookup[$pr->projname]) ? '<div class="no-image"></div>' : '<div class="homeimage homeimage-sprite" style="background-position:0 -'.($handle_lookup[$pr->projname]*INDEX_THUMB_H).'px"></div>')?></a>
				</td>
				<td>
					<strong><?=hesc(str_replace('_', ' ', $pr->projname))?></strong><?php if (SHOW_BLURBS) { ?>,
					<?=hesc($pr->shortdesc)?> 
					<?php } ?> 
					<a href="<?=hesc(BASEURL.urlencode($pr->projname))?>/" class="article-read-more">more...</a>
		<?php if (strlen($pr->subtag) || count($pr->tags)) { ?> 
					<br>
					<small>
						<?=hesc($pr->subtag)?> 
			<?php if (strlen($pr->subtag) && count($pr->tags)) { ?> 
						::
			<?php } ?> 
			<?php foreach($pr->tags as $tag) { ?> 
						<a class="tag tag-link" data-tag="<?=hesc($tag)?>"><?=hesc($tag)?></a>
			<?php } ?> 
					</small>
		<?php } ?> 
				</td>
			</tr>
	<?php } ?> 
		</table>
<?php

	$extra_head = '<link rel="canonical" href="'.hesc(BASEURL).'">';

	$index = template(SITE_TITLE, ob_get_clean(), $extra_head);
	file_put_contents(BASEDIR.'wwwroot/index.html', $index);
	
	// Done
}

function redirecthtml($target) {
	ob_start();
?>
<meta http-equiv="refresh" content="0; url=<?=hesc($target)?>">
<a href="<?=hesc($target)?>">Moved &raquo;</a>
<?php
	return ob_get_clean();
}

function buildredirects($redirects) {
	foreach($redirects as $oldname => $newname) {
	
		$page = redirecthtml(BASEURL.$newname.'/');
		
		// old format
		file_put_contents(BASEDIR.'wwwroot/'.$oldname.'.html', $page);
		
		// new format
		mkdir(BASEDIR.'wwwroot/'.$oldname);
		file_put_contents(BASEDIR.'wwwroot/'.$oldname.'/index.html', $page);
	}
}

function buildgosubpackages($packages) {
	foreach($packages as $path => $goGetStr) {
	
		$page = (
			'<meta name="go-import" content="'.hesc($goGetStr).'">'.
			"\n".
			redirecthtml(BASEURL)
		);
		
		// new directory format only
		mkdir_all(BASEDIR.'wwwroot/'.$path);
		file_put_contents(BASEDIR.'wwwroot/'.$path.'/index.html', $page);
	
	}
}
